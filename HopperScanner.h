/* 
 * File:   HopperScanner.h
 * Author: Jedan
 * 
 * Summary:
 *      This contains the class that makes reading a value from the hopper 
 *      easier. This class allows reading the hopper to be modular.
 */

#ifndef HOPPERSCANNER_H
#define	HOPPERSCANNER_H

#include <Arduino.h>
#define DATE 0
#define TIME 1
#define HOPPERWT 2
#define WORM 3
#define MOLD 4
#define GREEN 5
#define MOT 6
#define LU 7
#define SPARE_1 8
#define SPARE_2 9

class HopperScanner {
public:

    HopperScanner(HardwareSerial* inputSerial, String* fieldNames, bool* fieldsOn)
    : hopperSerial(inputSerial), NODATA(255), incoming(255), weightFields(fieldNames),
    weightFieldIsOn(fieldsOn) {

    };

    //Scanning functions
    void ReadHopperWeights();
    String getNextWeightLine();


    //get Field Functions
    String getDate();
    String getTime();
    String getHopperWeight();
    String getWorm();
    String getMold();
    String getGreen();
    String getMot();
    String getLimitedUse();
    String getOtherDefect1();
    String getOtherDefect2();
    String getField(int field);

    //public variables - Flags
    bool motIsDirt;
    bool motIsExtra;
    bool motIsBoth;



    //===============================================================
private:
    //functions - categorizing functions
    bool isLoadNumber(String& input);
    bool isDate(String& input);
    bool isTime(String& input);
    bool timeIsPm(String time);
    String correctTimeFormat(String time);
    String correctDateFormat(String date);
    String getMeasurementFromString(String& input);
    String getCategoryFromString(String& input);
    int countWeightsExpected();
    bool fieldIsDirt(String& input);
    bool fieldIsBoth(String& input);
    bool fieldIsExtra(String& input);
    bool weightsyntaxRecognition(String& input);

    //Serial buffer
    HardwareSerial* hopperSerial;

    //Fields
    String Fields[10];

    //Constant values
    byte NODATA;
    byte incoming;

    String* weightFields;
    bool* weightFieldIsOn;
};

#endif	/* HOPPERSCANNER_H */


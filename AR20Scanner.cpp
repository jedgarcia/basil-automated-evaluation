/* 
 * File:   AR20Scanner.cpp
 * Author: Jedan Garcia
 * 
 * Summary:
 *      This contains all the functions needed to make reading from the ph a 
 *      very modular process.
 * 
 * Functions:
 *      1. AR20Scanner: constructor, makes sure the right serial port is used and
 *              it starts the unresponsive flag as false.
 * 
 *      2. enableUnresponsive: This sets the unresponsive flag to true.
 * 
 *      3. getPh: reads in the next ph measurement coming into the serial port.
 * 
 *      4. getNextLine: used in getPh to parse through the data coming into the
 *              serial port and only detect the actual ph measurement.
 */

#include "AR20Scanner.h"

AR20Scanner::AR20Scanner(HardwareSerial* inputSerial) : AR20Serial(inputSerial), unresponsive(false) {
    AR20Serial->begin(19200);
}



void AR20Scanner::enableUnresposive(){
    unresponsive = true;
}



String AR20Scanner::getPh() {
    unresponsive = false;
    String phLine = "";
    String startOfPhLineSyntax = "pH measurement:";
    AR20Serial->flushBuffer();

    int counter = 0; //This will count how many lines we've had to go through to get the ph
    while (1) {
        phLine = getNextLine();
        counter++;
        if (phLine.startsWith(startOfPhLineSyntax)) {
            String ph = phLine.substring(15, phLine.length());
            ph.trim();
            return ph;
        }

        if (counter > 100) {
            unresponsive = true;
            String unresponsive = "unresponsive";
            return unresponsive;
        }

    }

}



String AR20Scanner::getNextLine() {
    String buffer = "";
    String empty = " ";
    byte incoming;

    int count = 0;

    while (true) {
        count++;
        if (AR20Serial->available()) {
            incoming = AR20Serial->read();

            if (incoming == 10 || incoming == '\r' || incoming == '\n') {
                return buffer;
            } else {
                buffer += (char) incoming;
            }
        }
        if (count > 20000) {
            return String("");
        }

    }

}

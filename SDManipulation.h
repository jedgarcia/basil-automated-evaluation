/* 
 * File:   SDManipulation.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Summary:
 *      Functions that allow us to actually save the loads into some sort of 
 *      format.
 * 
 * Functions:
 *      1. getAppropriateFilename: it takes a string and returns the appropriate
 *              file name. This is because this function is used on temporary
 *              files that have been queued. The saving structure that is on
 *              the SD card seperates everything by folders. However when the
 *              load is just in a queue it is held elsewhere. So in order to 
 *              correctly determine where that temp file is supposed to go 
 *              permanently it must be opened and its contents interpreted.
 * 
 *      2. getAppropriateFolderPath: This is a function that returns the folder
 *              path. This is needed because when you try to make a file in the
 *              SD card, it will not write if the folders that you have in your 
 *              path are non-existant. (eg. you cannot write
 *              "LOADS/MARCH/11/load.txt" if the folders LOADS, MARCH, 11 do not
 *              exist. That is why this function returns which folders need to 
 *              be made.
 * 
 *      3. stringWithoutChar: This function takes a string and a character and
 *              removes every instance of that character in the string and
 *              then returns the string without the character.
 * 
 *      4. getMonthAndDayFromDate: takes the date and returns "mm/dd".
 * 
 *      5. writeCommToFile: writes the color to a certain file.
 *      
 *      6. writeRefracToFile: writes the refrac to a file.   
 * 
 *      7. writePhToFile: writes the ph to a file.
 * 
 *      8. writeWeightsToFile: writes the weights to a file.
 * 
 *      9. printFile: it prints out a file to a specified serial connection.
 * 
 *      10. loadFileIsDone: checks the data in a file to see if the load is done.
 *              returns true if it is done else returns false.
 * 
 *      11. checkWhichDataIsCompleted: this goes through the file and sets flags
 *              for which data has been completed in that file.
 * 
 *      12.getRegradePosition: returns the position in the file where the regrade
 *              should start from.
 * 
 *      13. skipToRegradePosition: skips the file pointer straight to where the
 *              regrade should happen.
 * 
 *      14. addToSD: adds a field to a specified file.
 * 
 *      15. copyRestOfFileTo: copies from the current position of the file pointer
 *              up until the end to another file that you specify.
 * 
 *      16. copyWholeFileFromTo: copies a file to another specified path.
 * 
 *      17. copyUntilCommaPosition: copies from the beginning of a file up until
 *              a specified comma position.
 * 
 *      18. copyFromCurrentUntilCommaPosition: starts copying from wherever the
 *              file pointer is at until a certain amount of commas have passed.
 * 
 *      19. goToCommaPosition: skips a file until a certain comma position.
 * 
 *      20. getValueFromCommaPosition: return the value located at a certain 
 *              comma value.
 * 
 *      21. dataIsPresent: returns true if the value is anything but -1. 
 *              a -1 in a certain field means that there is no data present
 *              in that field.
 *              
 */

#ifndef SDMANIPULATION_H
#define	SDMANIPULATION_H
#include "GlobalVariables.h"
#include "ForwardDeclarations.h"
#include "StringForSD.h"
#include "Algorithms.h"

String getAppropriateFilename(char* filePath) {
    File file = SD.open(filePath, FILE_READ);
    String correctFilePath = "LOADS/";

    //skip the station code
    readNextLoadValue(file);

    //get the load number
    int temporaryLoad = readNextLoadValue(file).toInt();

    //getting the date
    String date = readNextLoadValue(file);

    //putting the filepath together
    correctFilePath += getMonthAndDayFromDate(date);

    //putting together the name of the text file
    correctFilePath += stringWithoutChar(getMonthAndDayFromDate(date), '/');

    correctFilePath += "L";

    correctFilePath += temporaryLoad;

    correctFilePath += ".txt";

    return correctFilePath;
}

String getAppropriateFolderPath(char* filePath) {
    File file = SD.open(filePath, FILE_READ);
    String correctFilePath = "LOADS/";

    //skip the station code
    readNextLoadValue(file);

    //get the load number
    int temporaryLoad = readNextLoadValue(file).toInt();

    //getting the date
    String date = readNextLoadValue(file);

    //putting the filepath together
    correctFilePath += getMonthAndDayFromDate(date);

    return correctFilePath;
}

String stringWithoutChar(String input, char target) {
    String stringWithoutchar = "";
    for (int i = 0; i < input.length(); i++) {
        if (input[i] != target) {
            stringWithoutchar += input[i];
        }
    }
    return stringWithoutchar;
}

String getMonthAndDayFromDate(String date) {//returns "mm/dd/"
    int slashCounter = 0;
    for (int i = 0; i < date.length(); i++) {
        if (date[i] == '/')
            slashCounter++;
        if (slashCounter == 2) return date.substring(0, i + 1);
    }

    String notValid = "notValid/";
    return notValid;
}

void writeCommToFile(char* filepath, bool isRegrade) {
    addToSD(filepath, colorBuffer, 12, isRegrade);
}

void writeRefracToFile(char* filepath, bool isRegrade) {
    if (refracBuffer.length() > 4)
        addToSD(filepath, refracBuffer.substring(0, refracBuffer.length() - 1), 13, isRegrade);
    else
        addToSD(filepath, refracBuffer, 13, isRegrade);
}

void writePhToFile(char* filepath, bool isRegrade) {

    if (phBuffer.length() > 4)
        addToSD(filepath, phBuffer.substring(0, phBuffer.length() - 1), 14, isRegrade);
    else
        addToSD(filepath, phBuffer, 14, isRegrade);
}

void writeWeightsToFile(char* filepath, bool isRegrade) {

    String buffer = "";

    //load number
    buffer += LoadNumber;
    addToSD(filepath, buffer, 1, isRegrade);

    //Hopper Weight
    addToSD(filepath, RemoveCategoryFromWeight(weightBuffers[0]), 4, isRegrade);

    //WORM
    addToSD(filepath, RemoveCategoryFromWeight(weightBuffers[1]), 5, isRegrade);

    //MOLD
    addToSD(filepath, RemoveCategoryFromWeight(weightBuffers[2]), 6, isRegrade);

    //GREEN
    addToSD(filepath, RemoveCategoryFromWeight(weightBuffers[3]), 7, isRegrade);

    //MOT
    if (MotISBoth()) {
        String Y = "Y";
        addToSD(filepath, Y, 8, isRegrade); //dirt
        addToSD(filepath, Y, 9, isRegrade); //extra
    } else if (MotIsDirt()) {
        String Y = "Y";
        String N = "N";
        addToSD(filepath, Y, 8, isRegrade); //dirt
        addToSD(filepath, N, 9, isRegrade); //extra
    } else if (MotISExtra()) {
        String Y = "Y";
        String N = "N";
        addToSD(filepath, N, 8, isRegrade); //dirt
        addToSD(filepath, Y, 9, isRegrade); //extra
    } else {//mot is neither dirt nor extra
        String N = "N";
        addToSD(filepath, N, 8, isRegrade); //dirt
        addToSD(filepath, N, 9, isRegrade); //extra
    }
    //Mot Weight
    addToSD(filepath, RemoveCategoryFromWeight(weightBuffers[4]), 10, isRegrade);

    //LU
    addToSD(filepath, RemoveCategoryFromWeight(weightBuffers[5]), 11, isRegrade);

    //SPARE 1
    addToSD(filepath, RemoveCategoryFromWeight(weightBuffers[6]), 15, isRegrade);

    //SPARE 2
    addToSD(filepath, RemoveCategoryFromWeight(weightBuffers[7]), 16, isRegrade);

}

bool printFile(char* filePath, HardwareSerial& monitor) {

    for (int i = 0; i < 4; i++)
        monitor.println();

    File file = SD.open(filePath);
    if (!file) {
        Serial.println("file didnt open :(");
        return false;
    }

    while (file.available()) {
        monitor.print((char) file.read());
    }

    for (int i = 0; i < 4; i++)
        monitor.println();
    return true;
}

bool loadFileIsDone(char* filePath, bool isRegrade) {
    if (checkWhichDataIsCompleted(filePath, isRegrade)) {//if able to open the file
        for (int i = 0; i < 4; i++) {
            if (!SDContains[i])//if it is missing any measurement
                return false;
        }
    } else
        return false;

    //None of the measurements were missing and we were able to open the file
    return true;
}//loadFileIsDone

//Checks to see which measurements are done

bool checkWhichDataIsCompleted(char* filePath, bool loadIsRegrade) {
    int commaPosition = 0;
    String value;

    //This function goes through a file and sets flags depending on which information the file has
    File file = SD.open(filePath, FILE_READ); //automatically seeks to beginning

    if (!file)//checks to see if the file opened
        return false;

    //clears the contains flags
    setToFalse(SDContains, 4);


    for (int i = 0; i < 4; i++) {// look for the 4 different types of fields in the file
        if (loadIsRegrade)
            value = getValueFromCommaPosition(SDLocation[i], file, loadIsRegrade);
        else { //the load is not a regrade
            value = getValueFromCommaPosition(SDLocation[i], file, loadIsRegrade);
            Serial.println(value);
        }
        if (dataIsPresent(value))
            SDContains[i] = true;
    }

    file.close(); //closes the file
    return true; //success
}//CHECK WHICH DATA IS IN FILE

//does not advance the position, but does return the regrade position

int getRegradePosition(File& file) {
    //saves the old position
    int oldPosition = file.position();

    //this is where the regrade position will be stored
    int regradePosition = 0;

    //gets the regrade position
    while (file.read() != '\n') {
        ++regradePosition;
    } //skips until the regrade part

    //puts the file in its old position
    file.seek(oldPosition);

    //returns regrade position
    return ++regradePosition;
}

//seeks the file to the regrade position, returns that position

int skipToRegradePosition(File& file) {
    int regradePosition = 0;
    while (file.read() != '\n') {
        ++regradePosition;
    } //skips until the regrade part
    return ++regradePosition;
}


//This function requires a sorted array to be passed to it, and it also requires
//the commaposition array to match the messages[] array, sorted in order of
//comma position

void addToSD(char* filepath, StringForSD messages[], bool isRegrade, int sizeOfArray) {

    if (sizeOfArray == 0)
        return;

    //This sorts the strings and ints according to their commaPositions
    insertionSort<StringForSD>(messages, sizeOfArray);

    //Getting rid of any leading or trailing spaces in the messages
    for (int i = 0; i < sizeOfArray; i++) {
        messages[i].trim();

        Serial.print("Message: ");
        Serial.print(messages[i].message);
        Serial.print("   Position: ");
        Serial.println(messages[i].commaPosition);

    }

    //opening the file were adding to 
    File input = SD.open(filepath, FILE_WRITE);

    //opening the file we will use as a temporary
    SD.remove("temp.txt");
    File temp = SD.open("temp.txt", FILE_WRITE);



    //Copy the file until the earliest comma Position
    copyUntilCommaPosition(input, messages[0].commaPosition, temp, isRegrade);

    //Adding each part of the array into the different parts
    for (int i = 0; i < sizeOfArray; i++) {
        //goes to the right position for the message to be written
        goToCommaPosition(messages[i].commaPosition + 1, input, isRegrade);
        goToCommaPosition(messages[i].commaPosition, temp, isRegrade);

        //2. We are going to add our inserted message after that comma
        temp.print(messages[i].message);
        temp.print(F(","));
        //goToCommaPosition(messages[i].commaPosition + 1, temp, isRegrade);

        //have to copy the rest of the file up until the next point
        if (i != sizeOfArray - 1) {//dont need to do it on the last iteration
            copyFromCurrentUntilCommaPosition(input, messages[i + 1].commaPosition, temp, isRegrade);
        } else {//just copy the rest of the file over
            copyFromCurrentUntilCommaPosition(input, 24, temp, isRegrade);
        }

    }//this for loop ends in an awkward place thats why we have to go to a certain comma position after
    input.close();

    //4. Now temp contains all the original information and what we added,
    //now we just need to copy it back to the original
    //4a. remove everything in the original input file
    copyWholeFileFromTo(temp, filepath);

    Serial.print("Copying whole file to: ");
    Serial.println(filepath);

    //5. Now we have to delete the temporary file
    temp.close();
    SD.remove("temp.txt");
}

void addToSD(char* filepath, String message, int commaPosition, bool isRegrade) {

    //    if (isRegrade) {
    //        Serial.println(F("You have chosen to do regrade"));
    //    } else
    //        Serial.println(F("No regrade for you"));
    message.trim();

    File input = SD.open(filepath, FILE_WRITE);
    if (!input)Serial.println("file in addToSD() didn't open");

    if (commaPosition > 24) {
        Serial.print(F("Comma position: "));
        Serial.print(commaPosition);
        Serial.println(F(" is invalid. (0-24)"));
        return;
    }

    //The way that we are going to add something to the load file is as follows
    //This in essence overwriting the field    

    //1. We are going to copy everything up to a certain comma
    //  -if the load is a regrade we need to copy up until the regrade + comma position
    //  -otherwise we just need to copy until the comma position

    //1a. Need to create a temporary file
    SD.remove("temp.txt");
    File temp = SD.open("temp.txt", FILE_WRITE);

    //2a. Need to copy To the temporary file
    copyUntilCommaPosition(input, commaPosition, temp, isRegrade);
    //Serial.println(F("After copying part of file by comma position"));

    goToCommaPosition(commaPosition + 1, input, isRegrade);
    goToCommaPosition(commaPosition, temp, isRegrade);


    //2. We are going to add our inserted message after that comma
    temp.print(message);
    temp.print(F(","));
    goToCommaPosition(commaPosition + 1, temp, isRegrade);

    //3. Then copy the rest of the file over.
    copyRestOfFileTo(input, temp);
    input.close();

    //4. Now temp contains all the original information and what we added,
    //now we just need to copy it back to the original

    //4a. remove everything in the original input file
    copyWholeFileFromTo(temp, filepath);

    //5. Now we have to delete the temporary file
    temp.close();
    SD.remove("temp.txt");
}

void copyRestOfFileTo(File& input, File& destination) {
    //Assumes that the input file is already at the right destination

    //This makes sure we start writing at the end of the destination file
    destination.seek(destination.size());

    while (input.available()) {
        destination.print((char) input.read());
    }
}

void copyWholeFileFromTo(File& source, char* filepath) {
    //save old position in the osurce file
    int sourcePosition = source.position();
    source.seek(0);

    //clear the new file its going to be written into
    SD.remove(filepath);
    File destination = SD.open(filepath, FILE_WRITE);

    //copy over the info from the source to the new file
    while (source.available())
        destination.print((char) source.read());

    //restore the position in the source 
    source.seek(sourcePosition);

    //close the new file
    destination.close();
}

void copyUntilCommaPosition(File& source, int endingCommaPosition, File& destination, bool isRegrade) {

    int sourcePosition = source.position();
    int destinationPosition = destination.position();

    source.seek(0);
    if (isRegrade) {
        while ((incoming = (char) source.read()) != '\n') {
            destination.print((char) incoming);
        }
        destination.println();
    }

    for (int i = 0; i < endingCommaPosition; i++) {
        //have to make it copy over to the other file here
        destination.print(readNextLoadValue(source));
        destination.print(F(","));
    }

    //Settings the files back to what they were
    source.seek(sourcePosition);
    destination.seek(destinationPosition);
}

void copyFromCurrentUntilCommaPosition(File& source, int endingCommaPosition, File& destination, bool isRegrade) {
    if (isRegrade) {
        while ((incoming = (char) source.read()) != '\n') {
            destination.print((char) incoming);
        }
        destination.println();
    }
    for (int i = 0; i < endingCommaPosition; i++) {
        //have to make it copy over to the other file here
        destination.print(readNextLoadValue(source));
        destination.print(F(","));
    }
}

void goToCommaPosition(int commaPosition, File& file) {
    for (int i = 0; i < commaPosition; i++)
        readNextLoadValue(file);
}

void goToCommaPosition(int commaPosition, File& file, bool isRegrade) {
    file.seek(0);
    if (isRegrade) {
        //Serial.println(F("going to comma position regrade"));
        skipToRegradePosition(file); //This will skip until the regrade
        goToCommaPosition(commaPosition, file);
    } else {//if it is not a regrade
        //Serial.println(F("goint to comma position not a regrade"));
        goToCommaPosition(commaPosition, file);
    }
}

String getValueFromCommaPosition(int commaPosition, File & file, bool isRegrade) {
    //backs up the old position in the file
    int currentPosition = file.position();

    //The value will go into this string
    String value;

    //goes to the comma position to get the information from
    goToCommaPosition(commaPosition, file, isRegrade);

    //puts that info into the string
    value = readNextLoadValue(file);

    //returns the file to the previous position it had
    file.seek(currentPosition);

    //returns the value
    return value;
}

bool dataIsPresent(String value) {
    //data is defined by any value other than -1
    if (value.toInt() != -1)
        return true;
    else
        return false;
}

#endif	/* SDMANIPULATION_H */


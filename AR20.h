/* 
 * File:   AR20.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu 
 * 
 * Summary: This files contains all the functions that read the ph measurement
 *      at a certain interval, makes sure the measurement falls within a certain
 *      epsilon, and times out after a predefined timeout period.
 * 
 * Functions:
 *      1. MeasuringAR20: This is the main function for reading the ph since it
 *              makes sure the timeouts are upheld and that a certain epsilon is
 *              met.
 * 
 *      2. enableExitFlag: This function is called by a timer after a timeout
 *              period and it switches a flag that notifies MeasuringAR20 that
 *              the ph has timed out and it should exit without a result.
 *      
 *      3. readPh: reads the measurement from the ph meter.
 * 
 *      4. checkDifference: This function takes in two numbers and an epsilon
 *              and returns true if the difference of those two numbers is
 *              within that epsilon.
 *       
 * 
 */

#ifndef AR20_H
#define	AR20_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"
#include "AR20Scanner.h"



void MeasuringAR20() {

    phSerial.begin(2400); // AR 20 - 9600 baud rate

    for (int i = 0; i < 15; i++) {//clearing out the pH buffer
        phBuffer[i] = '\0';
    }

    Timer phTimer;
    int timeoutEvent = phTimer.after(PH_STABLE_TIMEOUT, enableExitFlag);

    int measurerEvent = phTimer.every(PH_READING_RATE, readPh);

    int phBlinkEvent = phTimer.every(200, redIndicatorBlink);

    digitalWrite(indicatorGreen, LOW);

    phReadingCount = 0;
    phReadings = new float[50];

    readPh();

    while (1) {
        phTimer.update();
        if (ExitFlag) {
            ExitFlag = false; //resets the flag
            delete phReadings;
            break;
        }
    }//while(1)

    phTimer.stop(timeoutEvent);
    phTimer.stop(measurerEvent);
    phTimer.stop(phBlinkEvent);

    digitalWrite(indicatorRed, LOW);
    digitalWrite(indicatorGreen, HIGH);
}//MeasuringAR20

//--------------------------------------------------------

void enableExitFlag() {
    ExitFlag = true;
}

//--------------------------------------------------------

void readPh() {

    AR20Scanner phScanner(&phSerial);
    phBuffer = phScanner.getPh();
    phReadings[phReadingCount] = atof(&phBuffer[0]);

    Serial.print("This is the ph: ");
    Serial.println(phBuffer);


    if (phScanner.unresponsive) {
        Serial.println("It is unresponsive");
        ExitFlag = true;
        phUnresponsive = true;
    }

    if (phReadingCount >= 1) {
        if (checkDifference(phReadings[phReadingCount],
                phReadings[phReadingCount - 1], PH_EPSILON)) {
            ExitFlag = true;
        }
    }

    phReadingCount++;
}//readPh())

//--------------------------------------------------------

bool checkDifference(float first_measure, float second_measure, float epsilon_F) {
    int first = 1000 * first_measure;
    int second = 1000 * second_measure;
    int epsilon = epsilon_F * 1000;

    int difference = first - second;
    difference = abs(difference);

    if (difference < epsilon)
        return true;
    else
        return false;

}//checkDifference()

#endif	/* AR20_H */


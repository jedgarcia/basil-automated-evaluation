/* 
 * File:   StringForSD.cpp
 * Author: Jedan Garcia 
 * 
 * Summary:
 *      Implementation of the StringForSD class. 
 *  
 */

#include "StringForSD.h"

StringForSD::StringForSD(String message_in, int commaPosition_in) {
    message = "";
    message += message_in;

    commaPosition = commaPosition_in;
}

StringForSD::StringForSD() {
}

StringForSD::StringForSD(char* message_in, int commaPosition_in) {
    message = "";
    message += message_in;

    commaPosition = commaPosition_in;
}

//copy consructor

StringForSD::StringForSD(const StringForSD& orig) {
    message = orig.message;
    commaPosition = orig.commaPosition;
}

void StringForSD::trim() {
    message.trim();
}

void StringForSD::add(String message_in, int commaPosition_in) {
    message = message_in;
    commaPosition = commaPosition_in;
}

void StringForSD::add(char* message_in, int commaPosition_in) {
    message = String(message_in);
    commaPosition = commaPosition_in;
}

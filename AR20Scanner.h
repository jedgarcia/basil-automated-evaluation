/* 
 * File:   AR20Scanner.h
 * Author: Jedan Garcia
 *
 * Summary:
 *      This is a class that makes reading from the ph as easy as calling one
 *      function. All this class needs is the serial port in which the 
 *      ph is connected.
 * 
 */

#ifndef AR20SCANNER_H
#define	AR20SCANNER_H
#include <Arduino.h>
#include <Timer.h>

class AR20Scanner {
public:
    AR20Scanner(HardwareSerial* inputSerial);
    String getPh();
    bool unresponsive;

private:
    HardwareSerial* AR20Serial;
    String getNextLine();
    void enableUnresposive();
};

#endif	/* AR20SCANNER_H */


/* 
 * File:   Button.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Summary:
 *      This contains functions related to the button and blinking indicator
 *              lights.
 * 
 * Functions:
 *      1. button1ISPressed: If the red button is pressed it will return a true.
 *    
 *      2. Blink: when called it toggle the voltage on whatever pin is passed
 *              into it. This can be used to make things blink by using it from
 *              within a timer and calling it at a certain interval.
 * 
 *      3. redIndicatorBlink: same as Blink but with a predefined pin or the red
 *              led indicator.
 * 
 *      4.greenIndicatorBlink: same as redindicatorBlink but predefines with the
 *              green led indicator.
 * 
 *      5. stopBlinkEvent: Switches a flag to tell the machine to stop blinking
 *              elsewhere in code.
 */

#ifndef BUTTON_H
#define	BUTTON_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"



//==============================================================================
//BUTTON FUNCTIONS

bool button1IsPressed() {//checks if the measurement button is pressed
    if (digitalRead(button1Pin) == LOW)
        return true;
    return false;
}

bool blink_flag = false; //this flag is used to alternate between on and off

//This function is meant to be called from a timer every x milliseconds as to
//look like the led if blinking, by calling this function using a timer 
// we can blink and simulatenously do two things

void Blink(int pinNumber) {
    if (blink_flag) {
        digitalWrite(pinNumber, HIGH);
        blink_flag = false;
    } else {
        digitalWrite(pinNumber, LOW);
        blink_flag = true;
    }
};

void redIndicatorBlink() {
    digitalWrite(indicatorGreen, LOW);

    if (blink_flag) {
        digitalWrite(indicatorRed, HIGH);
        blink_flag = false;
    } else {
        digitalWrite(indicatorRed, LOW);
        blink_flag = true;
    }
}

void greenIndicatorBlink() {
    digitalWrite(indicatorRed, LOW);

    if (blink_flag) {
        digitalWrite(indicatorGreen, HIGH);
        blink_flag = false;
    } else {
        digitalWrite(indicatorGreen, LOW);
        blink_flag = true;
    }

}

void stopBlinkingEvent() {//takes a function that is passed onto the function
    if (CurrentlyBlinking) {
        CurrentlyBlinking = false;
        blinkTimer.stop(blinkDuringOngoing);
    }
}

#endif	/* BUTTON_H */


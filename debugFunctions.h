/* 
 * File:   debugFunctions.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Summary:
 *      This is the scratch pad where I started working on functions to 
 *              implement. It eventually grew and now just houses misc.
 *              functions. These should be relocated to other header files
 *              in further version.
 * 
 * Functions:
 *      1. setToFalse: sets a bool array of a certain size to false.
 *      
 *      2. getPhSettings: allows for users to set their own timeouts and epsilon
 *              and rate of reading for the ph machine.
 * 
 *      3. check_ph_settings: it simply shows the already set ph settings
 *              without allowing one to change them.
 * 
 *      4. getRidOfSpaces: gets rid of spaces in a string by reference.
 * 
 *      5. SerialFlush: flushes the incoming serial buffer.
 *      
 *      6.FlushAllSerial: flushes all the incoming serial buffers.
 * 
 *      7. turnIndicator: pass this function either a "GREEN" or "RED" to change
 *              the indicator light to either red or green.
 * 
 *      8. AtLeastOneListenFlagIsEnabled: returns true if at least one listen 
 *              flag is set.
 * 
 *      9. makeSureDateIsInTheFile: it makes sure that the load file it is 
 *              passed has the date in it.
 * 
 *      10. queueMenu: This is the menu where you can view which loads are still
 *              incomplete and it also allows you to send them to the master
 *              regardless.
 */

#ifndef DEBUGFUNCTIONS_H
#define	DEBUGFUNCTIONS_H

#include "GlobalVariables.h"
#include "ForwardDeclarations.h"
#include "SDManipulation.h"
#include "SimpleVector.h"
#include "SDFunctions.h"

//==============================================================================
//Debug Functions 

void setToFalse(bool array[], int size) {
    for (int i = 0; i < size; i++) {
        array[i] = false;
    }
}

//***************************************

void getPhSettings() { //this function allows the user to input the ph settings they want then it stores it into eeprom
    //dont worry too much about this

    while (1) { //getting valid rate for reading the ph meter
        lcd_clear();
        lcd_println(L1Ta, F("Rate(sec): "));

        numberPad(L1Ta + 12, 1, rate); //numberpad allows for a user to enter in numbers at a certain part of the screen
        //all you have to do is specify where on the screen, how many digits to accept,
        //and give a char buffer to store the response in


        PH_READING_RATE = atoi(rate)*1000; //converting rate from seconds to milliseconds

        if (PH_READING_RATE < 3000) {
            lcd_println(L1Ta, F("Rate Too Low"));
            delay(750);
        } else {
            //EEPROM_writeAnything(PH_READING_RATE_A, PH_READING_RATE);
            break;
        }
    }


    lcd_print(L2Ta, F("Epsilon(.xxx): "));
    lcd_print(L2Ta + 15, "0.");
    numberPad(L2Ta + 17, 3, &(epsilon[2]));
    PH_EPSILON = atof(epsilon);
    //EEPROM_writeAnything(PH_EPSILON_A, PH_EPSILON);


    while (1) {//getting a valid ph timeout from the user
        lcd_println(L3Ta, F("Timeout(sec): "));
        numberPad(L3Ta + 14, 2, timeout);
        PH_STABLE_TIMEOUT = atoi(timeout) * 1000L;
        if (PH_STABLE_TIMEOUT > 60000) {
            lcd_println(L3Ta, F("Timeout Too High"));
            delay(750);
        } else {
            //EEPROM_writeAnything(PH_STABLE_TIMEOUT_A, PH_STABLE_TIMEOUT);
            break;
        }
    }//while(1)


    lcd_print(L4Ta, F("Configuration Set"));
    saveCurrentConfiguration(MAIN_MENU_MODE);

    delay(1000);
}

void check_ph_settings() { //This function displays the saved ph settings neatly
    //dont worry too much about this
    lcd_clear();

    //buffers for the settings so we can print them out
    char rate_[15];
    char epsilon_[15];
    char timeout_[15];
    char dummy[15];
    int space = 0;

    itoa(PH_READING_RATE / 1000, rate_, 10); //divided by a thousand to get the rate in seconds (currently in milli seconds)
    itoa(PH_EPSILON * 1000, epsilon_, 10);
    itoa(PH_STABLE_TIMEOUT / 1000, timeout_, 10); // 10 as the last argument to denote that we want to convert from integer to alphanumeric decimal format

    if (PH_EPSILON * 1000 > 100) {
        strcpy(dummy, "");
        space = 0;
    } else if (PH_EPSILON * 1000 > 10) {
        strcpy(dummy, "0");
        space = 1;
    } else if (PH_EPSILON * 1000 < 10) {
        strcpy(dummy, "00");
        space = 2;
    }


    lcd_print(L1Ta, F("Rate(secs): "));
    lcd_print(L1Ta + 15, rate_);

    lcd_print(L2Ta, F("Epsilon: "));
    lcd_print(L2Ta + 15, F("0."));
    lcd_print(L2Ta + 17, dummy);
    lcd_print(L2Ta + 17 + space, epsilon_);


    lcd_print(L3Ta, F("Timeout(secs): "));
    lcd_print(L3Ta + 15, timeout_);

    lcd_print(L4Ta, F("Press Clear to exit"));

    while (getKey() != CLEAR);

}

void getRidOfSpaces(String& input) { // gets rid of all spaces in a string
    int length = input.length();
    String tempString = "";

    for (int i = 0; i < length; i++) {
        if (input.charAt(i) != ' ') {
            tempString += input.charAt(i);
        }
    }

    input = tempString;
}

void SerialFlush(HardwareSerial& chosenSerial) {
    while (chosenSerial.available()) {
        chosenSerial.read();
        delay(7);
    }
}

void FlushAllSerial() {
    SerialFlush(Serial);
    SerialFlush(Serial1);
    SerialFlush(Serial2);
    SerialFlush(Serial3);
}

void turnIndicator(String choice) {

    if (choice == "green" || choice == "GREEN") {
        digitalWrite(indicatorGreen, HIGH);
        digitalWrite(indicatorRed, LOW);
    } else if (choice == "red" || choice == "RED") {
        digitalWrite(indicatorGreen, LOW);
        digitalWrite(indicatorRed, HIGH);
    }
}

bool AtLeastOneListenFlagIsEnabled() {
    if (!listenPh && !listenColor && !listenRefrac && !listenWeight) {//user did not select any devices
        return false;
    } else
        return true;
}

void makeSureDateIsInTheFile(char* filepath) {

    File file = SD.open(filepath, FILE_WRITE);
    file.seek(0);

    int forSdCounter = 0;
    StringForSD sdInput[2];

    bool needToAddDate = false;
    bool needToAddTime = false;

    goToCommaPosition(2, file);

    //========================================================
    String date = readNextLoadValue(file);
    Serial.print(F("The date is: "));
    Serial.println(date);
    if (date.length() < 10) {
        needToAddDate = true;
    }

    String time = readNextLoadValue(file);
    Serial.print(F("The time is: "));
    Serial.println(time);
    if (time.length() < 8) {
        needToAddTime = true;
    }
    //================================================
    if (needToAddDate) {
        date = "";
        do {
            date = getDateFromDateTime();
        } while (date.length() < 10);

        sdInput[forSdCounter++].add(date, 2);
    }
    if (needToAddTime) {
        time = "";
        do {
            Serial.println("trying to get date");
            time = getTimeFromDateTime();
            Serial.println(time);
            delay(3000);
        } while (time.length() < 8);

        sdInput[forSdCounter++].add(time, 3);
    }

    if (needToAddDate || needToAddTime)
        addToSD(filepath, sdInput, false, forSdCounter);

    file.close();
}

void queueMenu() {
    lcd_clear();
    sendFinishedQueuedLoads(false);

    int number_of_files = loadQueue.getSize() + 1;

    String queueMenu[number_of_files];
    queueMenu[0] = "   Queue Menu";
    if (number_of_files == 1) {
        lcd_print(L3Ta, F("No Loads in Queue"));
        lcd_print(L4Ta, F("ENTER or Clear: exit"));

    }
    for (int i = 1; i < number_of_files; i++) {
        queueMenu[i] = "";
        if (i < 10)
            queueMenu[i] += "0";
        queueMenu[i] += i;
        queueMenu[i] += ". Load: ";
        queueMenu[i] += loadQueue.get(i - 1);
    }

    Menu2Digits(queueMenu, number_of_files);

    if (key == CLEAR || key == ENTER) {
        lcd_clear();
        lcd_print(L1Ta, F("Saving Changes..."));
        saveCurrentConfiguration(MAIN_MENU_MODE);
        return;
    }
    if (key < 1) {
        lcd_clear();
        lcd_print(L1Ta, "Incorrect Value");
        delay(1000);
    } else if (key > number_of_files) {
        lcd_clear();
        lcd_print(L1Ta, "Incorrect Value");
        delay(1000);
    } else {
        String filepath = "TEMP/";
        int loadNumber = loadQueue.get(key - 1);
        filepath += loadQueue.get(key - 1);
        filepath += ".txt";
        readFromLoadDataFromSD(&filepath[0]);
        if (key == BUTTONNUMBER) {//They want to send this load to gradestar

            makeSureDateIsInTheFile(&filepath[0]);

            File tempFile = SD.open(&filepath[0], FILE_WRITE);
            sendToGradestar(masterAndGradestarSerial, tempFile);

            String newFileName = getAppropriateFilename(&filepath[0]);
            String folders = newFileName.substring(0, 12);
            SD.mkdir(&folders[0]);

            Serial.print("The correct filepath is: ");

            Serial.println(&newFileName[0]);


            copyWholeFileFromTo(tempFile, &newFileName[0]);
            loadQueue.removeData(loadNumber);
            tempFile.close();

            SD.remove(&filepath[0]);
            saveCurrentConfiguration(MAIN_MENU_MODE);
            return;


        }
    }





}

#endif	/* DEBUGFUNCTIONS_H */


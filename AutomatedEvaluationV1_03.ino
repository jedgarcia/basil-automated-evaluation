/* 
 * File:   AutomatedEvaluationV1_03.cpp
 * Author: Jedan Garcia
 * 
 * Summary:
 *      This is main file for the project. This is the main caller of all
 *      the functions in the project. Everything stems from here.
 * 
 * Functions:
 *      1.setup: contains actions are to be ran once before the project actually
 *               starts.
 * 
 *      2. loadDefaults: loads default settings that are saved inside of the SD
 *              card. If it doesn't find the configurations folder it will just
 *              create one with the machine defaults.
 * 
 *      3.loop: The way the arduino lays out their programming makes this the 
 *              main loop. This is the main loop that runs on the machine
 *              which calls all other functions.
 */

#include <SoftwareSerial.h>     //used for lcd and Serial port 4
#include <Keypad.h>             //used for the keypad - duh...
#include <Timer.h>              //used for anything from blinking light to ph timeouts
#include <EEPROM.h>             //main arduino eeprom library
#include "EEPROMAnything.h"     //used to make writing different variable types to eeprom easier
#include <SD.h>                 //used to store data in sd cards
#include <Wire.h>               //used for communicating to the SDA and SCL
#include <RTClib.h>             //used for the real time clock
#include <string.h>             //used for strings
#include "SimpleVector.h"       //A basic vector class
#include "StringForSD.h"        //custom class for ease of writing to sd card in format needed
#include "AR20Scanner.h"        //custom class to read from the ph meter

//all forward declarations go in here in order to fix scoping issues,
//at the same time it allows modularity between interconnected functions
#include "ForwardDeclarations.h"

#include "HopperScanner.h"      //custom class to read from the hopper
#include "GlobalVariables.h"    //all global variables
#include "SDFunctions.h"        //all functions related to SD card functionality
#include "Lcd.h"                //contains all lcd related functions
#include "Button.h"             //contains button related functions
#include "GetSerial.h"          //contains getSerial() functions
#include "AR20.h"               //contains AR20 functions
#include "debugFunctions.h"     //contains a mix of functions, not just debugging
#include "LoadFunctions.h"      //contains load number functions
#include "MenuFunctions.h"      //contains menu functions
#include "RFM190.h"             //contains rfm190 functions
#include "MultipleDevices.h"    //contains functions that deal with measuring multiple devices
#include "MasterReceiverFunctions.h"    //contains the master functions
#include "Hopper.h"             //contains hopper functions
#include "LedColorMeter.h"      //cotains the LED color meter functions
#include "SettingsFunctions.h"  //contains functions to set custom settings
#include "RegradeFunctions.h"   //contains all functions related to regrading
#include "SDManipulation.h"     //contains functions that manipulate files in the sd card
#include "QueueFunctions.h"     //Contains functions that maintain the queue of loads
#include "Algorithms.h"         //contains template insertion sort (small data, it is efficient enough)
#include "StringForSD.h"        //contains class that makes it easier to load data into the sd files

void setup() {

    //Setting variables to their correct values
    for (int i = 0; i < 13; i++) //expectedDevices[]]
        expectedDevices[i] = true;


    for (int i = 0; i < 4; i++) //ReceivingDevicesFlags
        ReceivingDevicesFlags[i] = false;

    for (int i = 0; i < NUMBEROFMEASUREMENTS; i++)
        measurementsReceived[i] = false;

    // Hardware Serial UARTs
    Serial.begin(9600);
    Serial1.begin(9600);
    Serial2.begin(9600);
    Serial3.begin(9600); // digital serial connection
    masterAndGradestarSerial.begin(9600); //The communication to the master will be at baud rate 19200 because the communication to gradestar is at 19200
    delay(50);
    
    //These need to be here in order to save SRAM and instead use the arduino's
    //flash memory, the device would not have enough memory to run without this
    
    //Main Menu
    main_menu[0] = F("01. Master Mode"); //1st Arduino Function
    main_menu[1] = F("02. Measuring Mode"); //2nd Arduino Function
    main_menu[2] = F("03. Bright - 100%");
    main_menu[3] = F("04. Set Load Number");
    main_menu[4] = F("05. Change pH config");
    main_menu[5] = F("06. Check pH config");
    main_menu[6] = F("07. Regrade");
    main_menu[7] = F("08. Set station code");
    main_menu[8] = F("09. Delete SD info");
    main_menu[9] = F("10. Load Data-SD");
    main_menu[10] = F("11. Choose Devices");
    main_menu[11] = F("12. Set weight names");
    main_menu[12] = F("13. Queue Menu");

    //Device menu
    device_menu[0] = F("01. Nothing else");
    device_menu[1] = F("02. Go to main menu");
    device_menu[2] = F("03. RFM190 Refrac");
    device_menu[3] = F("04. AR20 pH meter");
    device_menu[4] = F("05. Colorimeter");
    device_menu[5] = F("06. VC505 WT Hopper");
    device_menu[6] = F("07. LED Color Meter");
    //end of menu - 10 items

    //The measurements_menu is results returned to you after measuring
    for (int i = 0; i < 12; i++)
        measurements_menu[i] = "";

    //Hardware Handshaking for RFM190
    pinMode(dsrPin1, OUTPUT);
    digitalWrite(dsrPin1, LOW);

    pinMode(dsrPin2, OUTPUT);
    digitalWrite(dsrPin2, LOW);

    pinMode(dsrPin3, OUTPUT);
    digitalWrite(dsrPin3, LOW);
    
    //setting up the button to take measurements - button on pin 51 - pull up button
    pinMode(button1Pin, INPUT);
    digitalWrite(button1Pin, HIGH);

    //Setting up Indicator Light - Red and Green 
    pinMode(indicatorRed, OUTPUT);
    pinMode(indicatorGreen, OUTPUT);

    //By default when the device is turned on the indicator light will shine green
    digitalWrite(indicatorRed, LOW);
    digitalWrite(indicatorGreen, LOW);

    //Setting up Lcd - Software Serial
    pinMode(lcdPin, OUTPUT);
    lcdSerial.begin(9600);

    //Seting up the master software serial
    pinMode(rxMasterPin, INPUT);
    pinMode(txMasterPin, OUTPUT);

    // This sets up the light in the measurement button
    pinMode(buttonLED, OUTPUT);
    digitalWrite(buttonLED, LOW);

    //Checks to make sure that an SD card is plugged in to the device
    if (!SD.begin(4))//do not remove me,this is not just debug code, is it actually initiating the sd card
        Serial.println(F("SD DID NOT BEGIN!"));
    else
        Serial.println(F("SD DID BEGIN"));
    
    
    //clear screen
    lcd_clear();

    //Don't remove this, if you do, then sometimes the LCD will
    //not show the menu when it is booted up, it will just show a blank screen
    //until you press something.
    lcd_print(L1Ta, F("STARTING UP..."));


    Wire.begin(); //this must be done in order to use SCL & SDA
    RTC.begin(); //You must begin the RTC 

    //displays the amount of free memory to the serial monitor
    Serial.print(F("freeMemory()="));
    Serial.println(freeMemory());

}//setup

void loadDefaults() {//loads the configuration settings in the sd card
    
    //This is the default path to the configuration file
    char filePath[] = "config.txt";
    
    if (SD.exists(filePath)) {//if a configuration file exists in the sd card    
        File file = SD.open(filePath, FILE_READ);
        readFromConfigFile(file);
    } else {//a configuration file was not found in the SD card
        //create a configuration file with the default settings
        createConfigFile();
        File file = SD.open(filePath, FILE_READ);
        readFromConfigFile(file);
    }
}



void loop() {//main function
    //A green indicator indicates no problems
    turnIndicator("green");

    if (first_run) {//first run, load defaults, load straight into last mode used
        loadDefaults();
        delay(1000);
    }
    
    lcd_clear();//makes space for the main menu to display

    //if it is not the first run display the menu and ask the user for their input
    if (!first_run) {
        arduinoFunction = mainMenu(main_menu, MAINMENUSIZE); //loads the user menu
    } else {
        first_run = false;
    }

    //arduinoFunction holds the user's response to the main menu, this switch
    //chooses what function the user has requested
    switch (arduinoFunction) { //Main menu
        case 0:// this is for when the SD card's function is 0 and it means
            //to start from this main menu, do not delete
            break;
        case 1:
            masterReceiver(); //master mode
            break;
        case 2:
            MeasuringMultiple(); //mode to measure multiple
            break;
        case 3:
            restoreBrightness(); //restores lcd to full brightness
            break;
        case 4:
            SetLoadNumber(); //menu to set the load number
            break;
        case 5:
            getPhSettings(); //In order to change ph settings
            break;
        case 6:
            check_ph_settings();//merely shows the ph settings
            break;
        case 7:
            regrade();//regrade option for loads
            break;
        case 8:
            setStationCode();//changes the station code the machine is set on
            break;
        case 9://deletes everything on the sd card and then boots up the defaults
            DeleteSDCard();
            createConfigFile();
            break;
        case 10://this is to view a specific load saved in the sd card
            readFromLoadDataFromSD();
            break;
        case 11://This is for the master box, it changes what devices the master
            //should expect to receive before marking a load as "complete"
            setDevicesMasterIsExpecting();
            break;
        case 12://Reading from the weight hopper is done as if this box was a printer
            //in order to differentiate between the different categories we need
            //to know how the specific station the box is being used in has set up
            //their labels in their weight hopper
            HopperSettings();
            break;
        case 13://This will display all the loads that are in the queue (not finished yet)
            //This also gives you the option to view the (incomplete) data and determine
            //if you would like to send off that data through gradestar
            queueMenu();
            break;
        default://If the user entered a value out of the bounds of the functions
            //available, an error message pop up on the screen
            IncorrectValueEntered();
            break;
    }
}//loop

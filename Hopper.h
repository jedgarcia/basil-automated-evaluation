/* 
 * File:   Hopper.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Summary:
 *      This file contains all the secondary weight functions that are not
 *              necessarily for measuring, but also for modifying settings
 *              related to the weight hopper.
 * 
 * Functions:
 *      1.readFromHopper: This is the main function that houses reading from
 *              the weight hopper and making good use of the hopperScanner
 *              class.
 * 
 *      2. displayWeightFieldInfo: displays some of the settings that go along 
 *              with the weight hopper.
 * 
 *      3. HopperSettings: houses a menu that the user can interact with to 
 *              change the settings that relate to the hopper in the arduino.
 * 
 *      4. customizeStringByScrolling: allows you to modify a label of the 
 *              hopper by using the directional arrows to change each letter
 *              one at a time (since the arduino does not have a full keyboard).
 * 
 *      5. removeCategoryFromWeight: takes in a string and only returns the
 *              category contained in that string. For example if the string
 *              passed in was MOT: 15.2, the function would return MOT.
 *      
 *      6. countWeightsExpected: returns how many weights are expected by us
 *              from the hopper.
 * 
 *      7. countmasterWeightsExpected: returns how many weights are expected by
 *              the master for us to send.
 * 
 *      9. isMotExtra: returns true if mot is EXTRA.
 *      
 *      10. isMotBoth: returns true is mot is BOTH.
 * 
 * 
 */

#ifndef HOPPER_H
#define	HOPPER_H

#include "ForwardDeclarations.h"
#include "GlobalVariables.h"
#include "HopperScanner.h"

void readFromHopper() {
    HopperScanner HopperScan(&weightSerial, weightFields, weightFieldIsOn);
    delay(50);
    
    HopperScan.ReadHopperWeights();

    int counter = 0;
    for (int i = 0; i < 10; i++) {
        if (weightFieldIsOn[i]) {
            weightBuffers[i] = HopperScan.getField(i);
            measurements_menu[WEIGHTLINE + counter++] = HopperScan.getField(i);
            Serial.println(HopperScan.getField(i));
        }
    }
    SerialFlush(weightSerial);
}


//This is used to change the labels in the hopper settings, this gives you
//a function that can take a variable title and index of the weight to change
//a specific weight hopper label. 
void displayWeightFieldInfo(const __FlashStringHelper * Title, int weightFieldIndex, bool manipulateString) {
    
    lcd_clear();
    lcd_print(L1Ta, F("   Edit "));
    lcd_print(L1Ta + 8, Title);

    if (manipulateString) {//if you selected you wanted to change the string
        lcd_print(L2Ta, F("-"));
        lcd_print(L2Ta + 1, weightFields[weightFieldIndex]);
        lcd_print(L4Ta, F("Press ENTER to edit"));
    }

    //This next if-else allows us to toggle labels on and off
    if (weightFieldIsOn[weightFieldIndex]) {
        lcd_print(L3Ta, F("ON-Button to toggle"));
    } else {
        lcd_print(L3Ta, F("OFF-Button to toggle"));
    }

    
    do {//The user will be able to use the button to toggle labels on and off
        getKey();
        if (button1IsPressed()) {
            weightFieldIsOn[weightFieldIndex] = !weightFieldIsOn[weightFieldIndex];
            if (weightFieldIsOn[weightFieldIndex]) {
                lcd_print(L3Ta, F("                    "));
                lcd_print(L3Ta, F("ON-Button to toggle"));
                delay(250);
            } else {
                lcd_print(L3Ta, F("                    "));
                lcd_print(L3Ta, F("OFF-Button to toggle"));
                delay(250);
            }
        }
    } while (key != ENTER && key != CLEAR);

    //If you have asked for this label to be manipulated (by passing in manipulateString as true)
    //Then the user will be able to edit the title using the up and down arrows
    //to scroll through the characters in the alphabet and numbers 0-9
    if (manipulateString) {
        if (key == ENTER) {//the user wants to edit
            lcd_clear();
            lcd_print(L1Ta, "  Edit Using Arrows");
            lcd_print(L4Ta, F("2ND-Back  ENTER-Next"));
            weightFields[weightFieldIndex] = customizeStringByScrolling(L2Ta, weightFields[weightFieldIndex]); //sets the Spare1Custom to what the user inputs
        }
    }
}


//Main Hopper Settings where you can select which labels to change
void HopperSettings() {
    byte hopperSettingsSize = 11;
    const __FlashStringHelper * hopperSettings[11];

    hopperSettings[0] = F("   Choose to edit");

    while (true) {
        if (weightFieldIsOn[HOPPERWT]) {
            hopperSettings[1] = F("0.*Hopper Wt");
        } else {
            hopperSettings[1] = F("0. Hopper Wt");
        }


        if (weightFieldIsOn[WORM]) {
            hopperSettings[2] = F("1.*WORM");
        } else {
            hopperSettings[2] = F("1. WORM");
        }


        if (weightFieldIsOn[MOLD]) {
            hopperSettings[3] = F("2.*MOLD");
        } else {
            hopperSettings[3] = F("2. MOLD");
        }


        if (weightFieldIsOn[GREEN]) {
            hopperSettings[4] = F("3.*GREEN");
        } else {
            hopperSettings[4] = F("3. GREEN");
        }


        if (weightFieldIsOn[MOT]) {
            hopperSettings[5] = F("4.*MOT");
        } else {
            hopperSettings[5] = F("4. MOT");
        }


        if (weightFieldIsOn[LU]) {
            hopperSettings[6] = F("5.*LU");
        } else {
            hopperSettings[6] = F("5. LU");
        }


        if (weightFieldIsOn[SPARE_1]) {
            hopperSettings[7] = F("6.*Other Defect 1");
        } else {
            hopperSettings[7] = F("6. Other Defect 1");
        }



        if (weightFieldIsOn[SPARE_2]) {
            hopperSettings[8] = F("7.*Other Defect 2");
        } else {
            hopperSettings[8] = F("7. Other Defect 2");
        }

        if (weightFieldIsOn[DATE]) {
            hopperSettings[9] = F("8.*Date");
        } else {
            hopperSettings[9] = F("8. Date");
        }

        if (weightFieldIsOn[TIME]) {
            hopperSettings[10] = F("9.*Time");
        } else {
            hopperSettings[10] = F("9. Time");
        }

        //display the menu here
        Menu(hopperSettings, hopperSettingsSize);

        //key is the response from the user
        switch (key) {
            case '0'://Hopper Wt 
                displayWeightFieldInfo(F("Hopper Wt"), HOPPERWT, true);
                break;
            case '1'://Worm
                displayWeightFieldInfo(F("WORM"), WORM, true);
                break;
            case '2'://MOLD
                displayWeightFieldInfo(F("MOLD"), MOLD, true);
                break;
            case '3'://Green
                displayWeightFieldInfo(F("GREEN"), GREEN, true);
                break;
            case '4'://Mot
                displayWeightFieldInfo(F("MOT"), MOT, true);
                break;
            case '5'://LU
                displayWeightFieldInfo(F("LU"), LU, true);
                break;
            case '6'://Other Defect 1
                displayWeightFieldInfo(F("Other Defect 1"), SPARE_1, true);
                break;
            case '7'://Other Defect 2
                displayWeightFieldInfo(F("Other Defect 2"), SPARE_2, true);
                break;
            case '8'://Other Defect 2
                displayWeightFieldInfo(F("Date"), DATE, false);
                break;
            case '9'://Other Defect 2
                displayWeightFieldInfo(F("Time"), TIME, false);
                break;

            case ENTER:
            case CLEAR:
                lcd_clear();
                lcd_print(lines[0], F("Saving changes..."));
                sendFinishedQueuedLoads(false);
                saveCurrentConfiguration(MAIN_MENU_MODE);
                return;
                break;
        }//switch(key))
    }//while(true)
}//hopperSettings


//When editing the labels, an asterisk appears under the selected character,
//also the characters change depending on the arrow pressed, this is the function
//that handles that
String customizeStringByScrolling(byte location, String customString) {

    String backupString = customString;

    //This is seeing where we should put the line that contains the asterisks
    //which signal which character you are editing, if you chose to do the customization
    //on the last line, then the asterisks will be places in the third.
    byte asteriskLine;
    if (location >= L1Ta && location <= 147) {
        asteriskLine = L2Ta;
    } else if (location >= L2Ta && location <= 211) {
        asteriskLine = L3Ta;
    } else if (location >= L3Ta && location <= 167) {
        asteriskLine = L4Ta;
    } else if (location >= L4Ta && location <= 231) {
        asteriskLine = L3Ta;
    }
    //Serial.println(asteriskLine);

    //Turn the string into all upper case
    customString.toUpperCase();

    //Fill in the empty white space up to 20 characters
    while (customString.length() < 20) {
        customString += " ";
    }

    lcd_print(location, customString);

    for (int i = 0; i < 20; i++) {//entering 20 characters

        //clears the line and then puts the asterisk in  its rightfull place
        lcd_print(asteriskLine, F("                  "));
        lcd_print(asteriskLine + i, F("*"));

        do {//Enter signals to go to next letter

            //listen if the user wants to scroll or presses enter or help or clear
            getKey();

            if (key == UP) {//want to decrease the ascii value
                Serial.println(F("Pressed up"));
                Serial.println(i);



                if (customString.charAt(i) > '0' && customString.charAt(i) <= '9') {//you can increment and decrease and be within numbers
                    customString[i] = customString[i] - 1;
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) > 'A' && customString.charAt(i) <= 'Z') {//you can increment and decrease and be within alphanumeric
                    customString[i] = customString[i] - 1;
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) == 'A') {//going up when on A you roll into 9
                    customString[i] = '9';
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) == '0') {
                    customString[i] = ' ';
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) == ' ') {
                    customString[i] = 'Z';
                    lcd_print(location + i, customString.charAt(i));
                }
            }//They want to go up



            if (key == DOWN) {//want to increase the ascii value
                Serial.println(F("PRESSED DOWN"));
                Serial.println(i);
                if (customString.charAt(i) >= '0' && customString.charAt(i) < '9') {//you can increment and decrease and be within numbers
                    customString[i] = customString.charAt(i) + 1;
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) >= 'A' && customString.charAt(i) < 'Z') {//you can increment and decrease and be within alphanumeric
                    customString[i] = customString.charAt(i) + 1;
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) == 'Z') {//going up when on A you roll into 9
                    customString[i] = ' ';
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) == '9') {
                    customString[i] = 'A';
                    lcd_print(location + i, customString.charAt(i));
                } else if (customString.charAt(i) == ' ') {
                    customString[i] = '1';
                    lcd_print(location + i, customString.charAt(i));
                }
            }//want to increase the ascii value


            if (key == HELP) { //Help signals making the current character a space
                customString[i] = ' ';
                lcd_print(location + i, " ");
            }//key == HELP


            if (key == SECOND) {//SECOND - want to go to a previous character
                if (i > 0)
                    i--;
                lcd_print(asteriskLine, F("                    "));
                lcd_print(asteriskLine + i, F("*"));
            }//key = SECOND


            if (button1IsPressed()) {//If they press the button, they are done manipulating
                //The function will exit after editing 20 characters, or when they press the button
                return customString;
            }

            if (key == CLEAR) {
                return backupString;
            }

        } while (key != ENTER);

    }
    customString.trim();
    return customString;
}

String RemoveCategoryFromWeight(String weight) {

    int length = weight.length();
    String dummyString = "";

    bool hitSemiColon = false;
    for (int i = 0; i < length; i++) {
        if (weight.charAt(i) == ':' && !hitSemiColon) {
            hitSemiColon = true;
            continue;
        }
        if (hitSemiColon) {
            dummyString += weight.charAt(i);
        }
    }
    return dummyString;
}

bool MotIsDirt() {
    int length = weightBuffers[MOT].length();
    for (int i = 0; i < length; i++) {
        if (weightBuffers[MOT].charAt(i) == 'D' ||
                weightBuffers[MOT].charAt(i) == 'd') {

            return true;
        }
    }
    return false;
}

int countWeightsExpected() {
    int counter = 0;
    for (int i = 0; i < maximumHopperEntries; i++) {

        if (weightFieldIsOn[i])
            counter++;
    }
    return counter;
}

int countmasterWeightsExpected() {
    int counter = 0;
    for (int i = 0; i < maximumHopperEntries; i++) {

        if (masterWeightIsOn[i])
            counter++;
    }
    return counter;
}

bool MotISBoth() {
    int length = weightBuffers[MOT].length();
    for (int i = 0; i < length; i++) {
        if (weightBuffers[MOT].charAt(i) == 'B' ||
                weightBuffers[MOT].charAt(i) == 'B') {

            return true;
        }
    }
    return false;
}

bool MotISExtra() {
    int length = weightBuffers[MOT].length();
    for (int i = 0; i < length; i++) {
        if (weightBuffers[MOT].charAt(i) == 'E' ||
                weightBuffers[MOT].charAt(i) == 'E') {
            return true;
        }
    }
    return false;
}


#endif	/* HOPPER_H */


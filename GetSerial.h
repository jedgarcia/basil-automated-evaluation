/* 
 * File:   GetSerial.h
 * Author:  Jedan Garcia 
 * Email:   jedgarcia@ucdavis.edu
 *
 * Summary:
 *      This file contains getIncoming functions and getXXX functions. The get-
 *      incoming functions are names from 0-3 because they getincoming from a
 *      serial, serial1, serial2, or serial3. The getXXX functions get the next
 *      byte of incoming data from whatever the serial is that the specific 
 *      instrument is connected to.
 */

#ifndef GETSERIAL_H
#define	GETSERIAL_H



//==============================================================================
//getIncoming Functions


//***************************************
void getIncoming0() {
    //incoming is a global variable
    incoming = Serial.read();
}
//***************************************

void getIncoming1() {
    //incoming is a global variable
    incoming = Serial1.read();
}
//***************************************

void getIncoming2() {
    //incoming is a global variable
    incoming = Serial2.read();
}
//***************************************

void getIncoming3() {
    //incoming is a global variable
    incoming = Serial3.read();
}
//***************************************

void getRFM190() {
    //incoming is a global variable
    incoming = refracSerial.read();
}
//***************************************

void getAR20() {
    //incoming is a global variable
    incoming = phSerial.read();
}
//==============================================================================

#endif	/* GETSERIAL_H */

